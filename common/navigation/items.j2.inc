<nav
  class="flex flex-row gap-4 font-medium flex-wrap items-center justify-center text-center"
>
  <a
    class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all h-max block"
    href="{{ url_localized('index.html') }}"
    >
    {% trans %}Home{% endtrans %}
  </a>
  <a
    class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all h-max block"
    href="{{ url_localized('users.html') }}"
    >
    {% trans %}Users{% endtrans %}
  </a>
  <a
    class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all h-max block"
    href="{{ url_localized('merchants.html') }}"
    >
    {% trans %}Merchants{% endtrans %}
  </a>
  <!-- a
    class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all h-max block"
    href="{{ url_localized('fees.html') }}"
    >
    {% trans %}Fees{% endtrans %}
  </a -->
  <div class="contents md:hidden">
    {% include 'common/navigation/lang-select.j2.inc' %}
  </div>
</nav>
