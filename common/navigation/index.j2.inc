<a
  id="skip"
  tabindex="0"
  href="#maincontent"
  role="link"
  aria-label="Skip to the main content of the page"
  >{%trans%}Skip to main content{%endtrans%}</a
>
<header
  class="navigation-menu w-screen bg-white bg-opacity-90 backdrop-blur-xl sticky z-50 top-0 flex items-center justify-center py-4 px-8 md:px-16"
>
  <nav
    class="navbar navbar-desktop flex items-center justify-between w-full max-w-screen-xl md:flex-row flex-col gap-7"
  >
    <div
      class="left flex flex-wrap gap-7"
      style="min-width: 136px; min-height: 60.9px"
    >
      <a class="navbar-brand" href="{{ url_localized('index.html') }}"
        ><img
          src="{{ url_static('images/logo/taler-full.svg') }}"
          alt="Taler Operations SA"
          style="width: 136px; height: 60.9px"
      /></a>
      <div class="nav hidden md:contents">
        {% include "common/navigation/items.j2.inc" %}
      </div>
    </div>
    <div class="right flex flex-row max-w-full">
      <div class="nav contents md:hidden">
        {% include "common/navigation/items.j2.inc" %}
      </div>
      <div class="hidden md:contents">
        {% include 'common/navigation/lang-select.j2.inc' %}
      </div>
    </div>
  </nav>
</header>
