<div class="lang-switcher-container relative">
  <button
    class="dropdown-trigger no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all block h-max"
    type="button"
    aria-label="Language Switcher Trigger"
  >
    {{ lang_full }}&nbsp;<span class="text-gray-400 p-1 bg-black bg-opacity-10"
      >[{{ lang }}]</span
    >
  </button>
  <ul
    class="dropdown-menu hidden flex-col absolute w-max items-center justify-center left-[50%] -translate-x-1/2 p-4 bg-black bg-opacity-10 rounded-lg mt-2 backdrop-blur-md"
    aria-label="Language Options"
  >
    {% if lang != 'en' %}
    <li class="py-1">
      <a
        class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all block h-max"
        href="{{ self_localized('en') }}"
        >English
        <span class="text-gray-500 p-1 bg-black bg-opacity-10">[en]</span></a
      >
    </li>
    {% endif %} {% if lang != 'de' %}
    <li class="py-1">
      <a
        class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all block h-max"
        href="{{ self_localized('de') }}"
        >Deutsch
        <span class="text-gray-500 p-1 bg-black bg-opacity-10">[de]</span></a
      >
    </li>
    {% endif %} {% if lang != 'fr' %}
    <li class="py-1">
      <a
        class="no-underline hover:underline text-black text-opacity-50 hover:text-opacity-40 transition-all block h-max"
        href="{{ self_localized('fr') }}"
        >Fran&ccedil;ais
        <span class="text-gray-500 p-1 bg-black bg-opacity-10">[fr]</span></a
      >
    </li>
    {% endif %}
  </ul>
</div>
