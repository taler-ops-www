<footer
  class="dark bg-black text-white w-screen relative py-8 footer flex flex-col items-center justify-center"
>
  <div
    class="footer-top-outer flex px-8 py-8 sm:px-10 items-center justify-center w-full"
  >
    <div
      class="footer-top flex p-4 items-start max-w-max justify-center flex-1 gap-12 lg:max-w-screen-2xl flex-col lg:flex-row lg:items-start lg:justify-between"
    >
      <div class="footer-col flex-1">
        <div class="text-center flex flex-col items-center">
          <a href="{{ url_localized('index.html') }}" class="footlogo"
            ><img
              src="{{ url_static('images/unsorted/footlogo.png') }}"
              alt="Homepage"
              style="width: 206px; height: 85px; max-width: max-content"
          /></a>
          <p class="mt-3">
            Taler Operations AG, {% trans %}Switzerland{% endtrans %}<br />
            Höheweg 80<br />
            CH-2502 Biel, Switzerland
          </p>
        </div>
      </div>
      <div class="footer-col flex-1">
        <div class="footer-items-list">
          <h4 class="section-title">{% trans %}Quick Links{% endtrans %}</h4>
          <ul>
            <li>
              <a
                class="link no-clr"
                href="{{ url_localized('index.html') }}"
                alt="{% trans %}Taler Operations AG Homepage{% endtrans %}"
              >
                {% trans %}
                Home
                {% endtrans %}
              </a>
            </li>
            {#
            <!-- <li><a class="link no-clr" href="{{ url_localized('merchants.html') }}">{% trans %}Merchant Integration Guide{% endtrans %}</a></li>
							<li><a class="link no-clr" href="{{ url_localized('users.html') }}">{% trans %}User Installation Guide{% endtrans %}</a></li> -->
            #}
            <li>
              <a class="link no-clr"
                rel="noopener noreferrer"
                href="https://docs.taler.net/"
                alt="{% trans %}GNU Taler system documentation{% endtrans %}"
                target="_blank"
               >
                {% trans %}
                Documentation 🔗
                {% endtrans %}
              </a>
            </li>
            <li>
              <a class="link no-clr"
                rel="noopener noreferrer"
                href="https://exchange.taler-ops.ch/terms"
                alt="{% trans %}Taler Operations Terms of Service{% endtrans %}"
                target="_blank"
               >
               {% trans %}
               Terms of Service 🔗
               {% endtrans %}
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="footer-col flex-1">
        <div class="footer-items-list">
          <h4 class="section-title">{% trans %}Partners{% endtrans %}</h4>
          <ul>
            <li>
              <a
                href="https://ngi.taler.net"
                target="_blank"
                rel="noopener noreferrer"
                alt="{% trans %}NGI Taler EU consortium{% endtrans %}"
                class="link no-clr"
                >
              {% trans %}
              Taler consortium 🔗
              {% endtrans %}
              </a>
            </li>
            <li>
              <a
                href="https://bfh.ch/"
                target="_blank"
                alt="{% trans %}Bern University of Applied Sciences{% endtrans %}"
                rel="noopener noreferrer"
                class="link no-clr"
                >
                {% trans %}
                Bern&nbsp;University of Applied&nbsp;Sciences 🔗
                {%endtrans %}
              </a>
            </li>
            <li>
              <a
                href="https://netzbon.ch"
                target="_blank"
                alt="{% trans %}NetzBon: Regional currency Basel{% endtrans %}"
                rel="noopener noreferrer"
                class="link no-clr"
                >
                {% trans %}
                NetzBon 🔗
                {% endtrans %}
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="footer-col flex-1">
        <div class="footbox footadres">
          <h4 class="section-title">{% trans %}Contact{% endtrans %}</h4>
          <ul>
            <li>
              <a class="link no-clr phone" href="tel:0041442801200"
                >{% trans %}Tel:{% endtrans %} +41 44 280 1200</a
              >
            </li>
            <li>
              <a class="link no-clr" href="mailto:compliance@taler-ops.ch"
                >
                {% trans %}Compliance officer 📧{% endtrans %}
              </a>
            </li>
            <li>
              <a class="link no-clr" href="mailto:contact@taler-ops.ch"
                >
                {% trans %}General&nbsp;inquiries 📧{% endtrans %}
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div
    class="footer-bottom flex items-center justify-center flex-col text-center px-6 max-w-5xl"
  >
    <p>
      © {% trans %}Copyright{% endtrans %} 2024
      <a class="link"
        href="{{ url_localized('index.html') }}"
        >
        Taler Operations AG
      </a>.
      {% trans %}
        Licensed under the
        <a
          href="https://www.gnu.org/licenses/fdl-1.3.html.en"
          target="_blank"
          rel="noopener noreferrer"
          class="link"
        >
        GNU Free Documentation License 🔗
        </a>.
      {% endtrans %}
    </p>
    <p class="py-2">
      {% trans %}
      We do not assume any liability for the correctness of contents.
      We do not collect any data and do not use cookies.
      {% endtrans %}
    </p>
    <p>
      {% trans %}
      Our web server generates a log file with IP addresses of visitors.
      However, these are stored &amp; processed exclusively for troubleshooting and
      mitigating problems.
      {% endtrans %}
    </p>
  </div>
</footer>
