<script type="module">
  /*
    @licstart  The following is the entire license notice for the
    JavaScript code in this page.

    Copyright (C) 2014, 2015, 2016 GNUnet e.V.
    Copyright (C) 2017-2024 Taler Systems SA

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.

    @licend  The above is the entire license notice
    for the JavaScript code in this page.
  */
</script>

<link rel="icon" type="image/ico" href="{{ url_static('favicon.ico?v=2') }}" />
<link rel="alternate" hreflang="en" href="{{ self_localized('en') }}" />
<link rel="alternate" hreflang="fr" href="{{ self_localized('fr') }}" />
<link rel="alternate" hreflang="de" href="{{ self_localized('de') }}" />
<!-- <link rel="alternate" hreflang="it" href="{{ self_localized('it') }}" /> -->

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta
  name="viewport"
  content="width=device-width, initial-scale=1, shrink-to-fit=no"
/>

<script src="{{ url_static('js/show-on-js.ts') }}" defer></script>

<link href="{{ url_static('postcss/tailwind.css') }}" rel="stylesheet" />
<link href="{{ url_static('scss/global.scss') }}" rel="stylesheet" />
