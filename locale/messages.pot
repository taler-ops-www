# Translations template for PROJECT.
# Copyright (C) 2025 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2025.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2025-01-18 22:12+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.16.0\n"

#: common/base.j2:24 common/base.j2:28
msgid "Taler Operations AG, Biel, Switzerland"
msgstr ""

#: common/footer.j2.inc:19
msgid "Switzerland"
msgstr ""

#: common/footer.j2.inc:27
msgid "Quick Links"
msgstr ""

#: common/footer.j2.inc:33
msgid "Taler Operations AG Homepage"
msgstr ""

#: common/footer.j2.inc:35 common/navigation/items.j2.inc:8
msgid "Home"
msgstr ""

#: common/footer.j2.inc:48
msgid "GNU Taler system documentation"
msgstr ""

#: common/footer.j2.inc:51
msgid "Documentation 🔗"
msgstr ""

#: common/footer.j2.inc:60
msgid "Taler Operations Terms of Service"
msgstr ""

#: common/footer.j2.inc:63
msgid "Terms of Service 🔗"
msgstr ""

#: common/footer.j2.inc:73
msgid "Partners"
msgstr ""

#: common/footer.j2.inc:80
msgid "NGI Taler EU consortium"
msgstr ""

#: common/footer.j2.inc:83
msgid "Taler consortium 🔗"
msgstr ""

#: common/footer.j2.inc:92
msgid "Bern University of Applied Sciences"
msgstr ""

#: common/footer.j2.inc:96
msgid "Bern&nbsp;University of Applied&nbsp;Sciences 🔗"
msgstr ""

#: common/footer.j2.inc:105
msgid "NetzBon: Regional currency Basel"
msgstr ""

#: common/footer.j2.inc:109
msgid "NetzBon 🔗"
msgstr ""

#: common/footer.j2.inc:119 template/terms.html.j2:631
msgid "Contact"
msgstr ""

#: common/footer.j2.inc:123
msgid "Tel:"
msgstr ""

#: common/footer.j2.inc:129
msgid "Compliance officer 📧"
msgstr ""

#: common/footer.j2.inc:135
msgid "General&nbsp;inquiries 📧"
msgstr ""

#: common/footer.j2.inc:147
msgid "Copyright"
msgstr ""

#: common/footer.j2.inc:153
msgid ""
"Licensed under the <a "
"href=\"https://www.gnu.org/licenses/fdl-1.3.html.en\" target=\"_blank\" "
"rel=\"noopener noreferrer\" class=\"link\" > GNU Free Documentation "
"License 🔗 </a>."
msgstr ""

#: common/footer.j2.inc:166
msgid ""
"We do not assume any liability for the correctness of contents. We do not"
" collect any data and do not use cookies."
msgstr ""

#: common/footer.j2.inc:172
msgid ""
"Our web server generates a log file with IP addresses of visitors. "
"However, these are stored &amp; processed exclusively for troubleshooting"
" and mitigating problems."
msgstr ""

#: common/navigation/index.j2.inc:7
msgid "Skip to main content"
msgstr ""

#: common/navigation/items.j2.inc:14
msgid "Users"
msgstr ""

#: common/navigation/items.j2.inc:20
msgid "Merchants"
msgstr ""

#: common/navigation/items.j2.inc:26
msgid "Fees"
msgstr ""

#: template/fees.html.j2:2
msgid "Taler Operations: Fees"
msgstr ""

#: template/fees.html.j2:5 template/fees.html.j2:9
msgid "Fees collected by Taler Operations AG."
msgstr ""

#: template/fees.html.j2:21
msgid "Wire transfer fee"
msgstr ""

#: template/fees.html.j2:26
msgid "Charge: <b>0.1&nbsp;CHF</b> per transfer."
msgstr ""

#: template/fees.html.j2:27
msgid "Merchants can bundle multiple payments to incur only one fee."
msgstr ""

#: template/fees.html.j2:28
msgid "Fee Deduction: Directly from the transferred amount."
msgstr ""

#: template/fees.html.j2:35
msgid "Deposit fees"
msgstr ""

#: template/fees.html.j2:40
msgid "Charges vary based on coin denomination used."
msgstr ""

#: template/fees.html.j2:41
msgid "Merchants can choose to cover typical fees to ensure customers rarely pay."
msgstr ""

#: template/fees.html.j2:42
msgid "Customers are informed of fees they pay before each transaction."
msgstr ""

#: template/fees.html.j2:50
msgid "Coin value range"
msgstr ""

#: template/fees.html.j2:51
msgid "Fee per coin"
msgstr ""

#: template/fees.html.j2:70
msgid ""
"Merchants can set the merchant backend to cover <a "
"href=\"/images/stefan.svg\" target=\"_blank\" rel=\"noopener noreferrer\""
" >customary fees &#128185; 🔗</a>."
msgstr ""

#: template/fees.html.j2:85
msgid "Bounce fee"
msgstr ""

#: template/fees.html.j2:90
msgid "Charge: <b>0.1&nbsp;CHF</b> for returned funds."
msgstr ""

#: template/fees.html.j2:91
msgid ""
"Reasons: Unclaimed funds after 4 weeks, or incorrect wire transfer "
"details."
msgstr ""

#: template/fees.html.j2:92
msgid "Avoidance: Ensure correct details and wallet connectivity within a month."
msgstr ""

#: template/fees.html.j2:100
msgid "Possible loss of e-money due to expiration"
msgstr ""

#: template/fees.html.j2:105
msgid "Validity: Taler eCHF valid for <b>1&nbsp;year</b>."
msgstr ""

#: template/fees.html.j2:107
msgid ""
"Auto-renewal: Wallets auto-exchange expiring eCHF <b>one&nbsp;month</b> "
"prior."
msgstr ""

#: template/fees.html.j2:108
msgid "Requirement: Regular wallet connectivity to prevent loss of funds."
msgstr ""

#: template/index.html.j2:5 template/index.html.j2:9 template/index.html.j2:28
msgid "Taler Operations runs the GNU Taler payment system in Switzerland."
msgstr ""

#: template/index.html.j2:23
msgid "Welcome to <span class=\"text-accent-pure\">Taler Operations</span>!"
msgstr ""

#: template/index.html.j2:34
msgid ""
"Taler Operations is not supervised by the Swiss Financial Market "
"Supervisory Authority (FINMA). Escrowed customer assets are not subject "
"to deposit insurance but held in a segregated bank account."
msgstr ""

#: template/index.html.j2:46
msgid "Swiss Franc coin with GNU head as face value"
msgstr ""

#: template/index.html.j2:57
msgid "Getting started with Taler"
msgstr ""

#: template/index.html.j2:65
msgid "happy user"
msgstr ""

#: template/index.html.j2:66
msgid "For Users"
msgstr ""

#: template/index.html.j2:68
msgid "Your data and money under your control."
msgstr ""

#: template/index.html.j2:77
msgid "Pay&nbsp;with&nbsp;Taler"
msgstr ""

#: template/index.html.j2:84
msgid "happy merchant"
msgstr ""

#: template/index.html.j2:85
msgid "For Merchants"
msgstr ""

#: template/index.html.j2:87
msgid "Easy to use, low fees, no compliance worries."
msgstr ""

#: template/index.html.j2:96
msgid "Integrate"
msgstr ""

#: template/merchants.html.j2:2
msgid "GNU Taler for Merchants"
msgstr ""

#: template/merchants.html.j2:5 template/merchants.html.j2:9
msgid ""
"This page explains different ways how merchants can accept GNU Taler "
"payments."
msgstr ""

#: template/merchants.html.j2:25 template/users.html.j2:18
msgid "Prerequisites"
msgstr ""

#: template/merchants.html.j2:30
msgid ""
"Here is what you need to get started to integrate Taler as a payment "
"option into your payment process:"
msgstr ""

#: template/merchants.html.j2:38
msgid ""
"Swiss bank account, ideally with support for <a target=\"_blank\" "
"href=\"https://www.ebics.org/\">EBICS 🔗</a>."
msgstr ""

#: template/merchants.html.j2:44
msgid ""
"<a target=\"_blank\" href=\"https://docs.taler.net/taler-merchant-"
"manual.html\" > Taler merchant backend 🔗 </a> configured with your Swiss "
"IBAN number."
msgstr ""

#: template/merchants.html.j2:64
msgid "⭳ Install GNU Taler merchant backend ⭳"
msgstr ""

#: template/merchants.html.j2:73
msgid "For in-person sales"
msgstr ""

#: template/merchants.html.j2:78
msgid ""
"The merchant backend can generate static QR codes customers can scan to "
"pay you."
msgstr ""

#: template/merchants.html.j2:84
msgid "You can hard-code a price or have the customer enter the price."
msgstr ""

#: template/merchants.html.j2:95
msgid "Watch tutorial 🎞"
msgstr ""

#: template/merchants.html.j2:115
msgid "For WooCommerce"
msgstr ""

#: template/merchants.html.j2:120
msgid "A payment plugin is availalbe to add GNU Taler support to WooCommerce."
msgstr ""

#: template/merchants.html.j2:126 template/merchants.html.j2:167
#: template/merchants.html.j2:209
msgid "You mostly need to configure access to your Taler merchant backend."
msgstr ""

#: template/merchants.html.j2:137
msgid "⭳ WooCommerce&nbsp;plugin ⭳"
msgstr ""

#: template/merchants.html.j2:146
msgid "Screenshot of Woocommerce settings dialog"
msgstr ""

#: template/merchants.html.j2:156
msgid "For Pretix"
msgstr ""

#: template/merchants.html.j2:161
msgid "A payment plugin is availalbe to add GNU Taler support to Pretix."
msgstr ""

#: template/merchants.html.j2:178
msgid "⭳ Pretix&nbsp;plugin ⭳"
msgstr ""

#: template/merchants.html.j2:188
msgid "Pretix logo"
msgstr ""

#: template/merchants.html.j2:198
msgid "For Joomla!"
msgstr ""

#: template/merchants.html.j2:203
msgid ""
"A payment plugin is availalbe to add GNU Taler support to Joomla! with "
"Payage."
msgstr ""

#: template/merchants.html.j2:220
msgid "⭳ Joomla!&nbsp;plugin ⭳"
msgstr ""

#: template/merchants.html.j2:229
msgid "Woocommerce Taler settings"
msgstr ""

#: template/merchants.html.j2:238
msgid "For points of sale"
msgstr ""

#: template/merchants.html.j2:240
msgid ""
"A point-of-sale App for Android can be used to quickly set up orders and "
"handle payments."
msgstr ""

#: template/merchants.html.j2:246
msgid ""
"The App was designed for restaurants, but might be useful for other types"
" of in-person sales."
msgstr ""

#: template/merchants.html.j2:257
msgid "App manual 🔗"
msgstr ""

#: template/merchants.html.j2:266
msgid "GNU Taler point-of-sale app user interface screenshot"
msgstr ""

#: template/merchants.html.j2:276
msgid "For developers"
msgstr ""

#: template/merchants.html.j2:281
msgid ""
"The merchant backend offers a simple REST API to integrate GNU Taler into"
" existing E-commerce systems."
msgstr ""

#: template/merchants.html.j2:287
msgid ""
"Documentation is available at <a target=\"_blank\" "
"href=\"https://docs.taler.net/\">docs.taler.net 🔗</a>."
msgstr ""

#: template/merchants.html.j2:297
msgid "Watch tutorials 🎞"
msgstr ""

#: template/terms.html.j2:2
msgid "Taler Operations AG - Terms of Service"
msgstr ""

#: template/terms.html.j2:6 template/terms.html.j2:10
msgid "The Taler Terms of Service"
msgstr ""

#: template/terms.html.j2:17 template/terms.html.j2:31
msgid "eNetzBon Terms of Service"
msgstr ""

#: template/terms.html.j2:19
msgid ""
"These <b>Terms of Service</b> are applicable to any <b>user or "
"merchant</b> making use of the Taler payment system for <b>eNetzBon</b>."
msgstr ""

#: template/terms.html.j2:634
msgid ""
"You can get into touch with Taler Operations AG via mail or email and on "
"the phone:"
msgstr ""

#: template/terms.html.j2:647
msgid "Reach Us"
msgstr ""

#: template/terms.html.j2:657
msgid ""
"Email Us</h4> <p> Get into touch with us via <br />one of the email "
"contacts below"
msgstr ""

#: template/terms.html.j2:670
msgid "Call Us"
msgstr ""

#: template/terms.html.j2:672
msgid "Have a question?"
msgstr ""

#: template/users.html.j2:2
msgid "GNU Taler for Users"
msgstr ""

#: template/users.html.j2:5 template/users.html.j2:9
msgid "This page explains how to use GNU Taler to make payments."
msgstr ""

#: template/users.html.j2:20
msgid "Here is what you need to get started paying with GNU Taler:"
msgstr ""

#: template/users.html.j2:26
msgid "<b>Swiss bank account</b> with an IBAN beginning with \"CH\","
msgstr ""

#: template/users.html.j2:31
msgid ""
"<b>Mobile phone number</b> beginning with \"+41\" to receive P2P "
"payments, and"
msgstr ""

#: template/users.html.j2:36
msgid ""
"<b>Installation</b> of a <a href=\"https://wallet.taler.net/\" "
"target=\"_blank\" rel=\"noopener noreferrer\" > Taler wallet 🔗 </a>."
msgstr ""

#: template/users.html.j2:46
msgid ""
"<a href=\"https://tutorials.taler.net/wallet\" target=\"_blank\" "
"rel=\"noopener noreferrer\" > Tutorials available 🎞 </a>."
msgstr ""

#: template/users.html.j2:65
msgid "⭳ Download GNU Taler wallet ⭳"
msgstr ""

#: template/users.html.j2:79
msgid "Select platform to view instructions for:"
msgstr ""

#: template/users.html.j2:86
msgid "Select instructions for Android"
msgstr ""

#: template/users.html.j2:103
msgid "Select instructions for iOS"
msgstr ""

#: template/users.html.j2:120
msgid "Select instructions for Firefox"
msgstr ""

#: template/users.html.j2:137
msgid "Select instructions for Chrome"
msgstr ""

#: template/users.html.j2:159 template/users.html.j2:196
#: template/users.html.j2:247 template/users.html.j2:297
#: template/users.html.j2:349 template/users.html.j2:402
#: template/users.html.j2:448 template/users.html.j2:504
#: template/users.html.j2:556
msgid "Step"
msgstr ""

#: template/users.html.j2:162
msgid "Add provider"
msgstr ""

#: template/users.html.j2:166
msgid ""
"As a first-time user, you need to add Taler Operations as a payment "
"service provider. The easiest way to do so is to <b>scan the QR code</b> "
"on the right or click it to open the link. This will also initiate the "
"withdrawal process."
msgstr ""

#: template/users.html.j2:174
msgid ""
"If you already have a payment service provider, you can withdraw digital "
"cash by clicking the \"add\" button on the \"balance\" screen of the "
"respective currency."
msgstr ""

#: template/users.html.j2:199
msgid "Enter amount"
msgstr ""

#: template/users.html.j2:205
msgid "Enter the desired amount to be withdrawn."
msgstr ""

#: template/users.html.j2:210
msgid "Afterwards, select \"Withdraw\" from my bank account."
msgstr ""

#: template/users.html.j2:219 template/users.html.j2:225
#: template/users.html.j2:231 template/users.html.j2:237
#: template/users.html.j2:321 template/users.html.j2:327
#: template/users.html.j2:333 template/users.html.j2:339
msgid "Wallet requesting amount to withdraw"
msgstr ""

#: template/users.html.j2:250
msgid "Accept terms"
msgstr ""

#: template/users.html.j2:254
msgid "Please read the terms of service and accept them."
msgstr ""

#: template/users.html.j2:259
msgid ""
"You will only need to do so the first time, or when the terms have been "
"updated."
msgstr ""

#: template/users.html.j2:269 template/users.html.j2:275
#: template/users.html.j2:281 template/users.html.j2:287
msgid "Wallet showing terms of service"
msgstr ""

#: template/users.html.j2:300
msgid "Review operation"
msgstr ""

#: template/users.html.j2:306
msgid "Review and confirm the operation."
msgstr ""

#: template/users.html.j2:311
msgid "Depending on the payment service provider, you may be shown fees."
msgstr ""

#: template/users.html.j2:352
msgid "Wire transfer"
msgstr ""

#: template/users.html.j2:358
msgid ""
"The wallet will show you the wire transfer subject, target account and "
"amount to transfer."
msgstr ""

#: template/users.html.j2:364
msgid ""
"Copy the information into your online banking application and wire the "
"funds to the payment service provider."
msgstr ""

#: template/users.html.j2:374 template/users.html.j2:380
#: template/users.html.j2:386 template/users.html.j2:392
msgid "Wallet showing wire transfer instructions"
msgstr ""

#: template/users.html.j2:404
msgid "Wait!"
msgstr ""

#: template/users.html.j2:407
msgid ""
"The wallet will automatically receive the digital cash once your wire "
"transfer has arrived at the payment service provider."
msgstr ""

#: template/users.html.j2:412
msgid ""
"Depending on your bank, this may take a few days. You do not need to keep"
" the wallet open."
msgstr ""

#: template/users.html.j2:420 template/users.html.j2:426
#: template/users.html.j2:432 template/users.html.j2:438
msgid "Wallet showing withdrawal status"
msgstr ""

#: template/users.html.j2:451
msgid "Shop"
msgstr ""

#: template/users.html.j2:457
msgid ""
"When shopping, the wallet may be launched automatically or you may need "
"to click a link, scan a QR code or even select the wallet extension "
"manually depending on the context."
msgstr ""

#: template/users.html.j2:464
msgid ""
"The QR code scanner button can also be held to manually enter a "
"\"<code>taler://</code>-URI\", for example if you received one via text "
"message."
msgstr ""

#: template/users.html.j2:475 template/users.html.j2:481
#: template/users.html.j2:487 template/users.html.j2:494
msgid "Wallet requesting taler://-URI"
msgstr ""

#: template/users.html.j2:507
msgid "Pay"
msgstr ""

#: template/users.html.j2:513
msgid ""
"The wallet will show you details about what you are buying, including the"
" price and applicable fees."
msgstr ""

#: template/users.html.j2:519
msgid "Simply press \"Pay\" to make the payment."
msgstr ""

#: template/users.html.j2:528 template/users.html.j2:534
#: template/users.html.j2:540 template/users.html.j2:546
msgid "Wallet requesting payment confirmation"
msgstr ""

#: template/users.html.j2:559
msgid "Manage"
msgstr ""

#: template/users.html.j2:565
msgid "The wallet keeps track of your transaction history."
msgstr ""

#: template/users.html.j2:570
msgid "You can delete transactions, erasing any trace that you were the buyer."
msgstr ""

#: template/users.html.j2:580 template/users.html.j2:586
#: template/users.html.j2:592 template/users.html.j2:598
msgid "Wallet showing receipt"
msgstr ""

#: template/users.html.j2:612
msgid "Frequently Asked Questions 🔗"
msgstr ""

