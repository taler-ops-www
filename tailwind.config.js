const defaultFonts =
  `system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif`.split(
    ', ',
  );
const ext = [
  'html',
  'tsx',
  'jsx',
  'ts',
  'js',
  'css',
  'scss',
  'sass',
  'postcss',
];
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    'rendered/**/*.{' + ext.join(',') + '}',
    'rendered/*.{' + ext.join(',') + '}',
  ],
  theme: {
    extend: {
      // things that are new go here
      colors: {
        accent: {
          pure: '#0042B3',
          // todo: add accent shades
        },
      },
    },
    // things to replace go below here:
    fontFamily: {
      sans: ['Montserrat', 'montserrat', 'ui-sans-serif', ...defaultFonts],
      serif: ['ui-serif', 'Georgia', ...defaultFonts],
      mono: ['ui-monospace', 'SFMono-Regular', 'Consola', 'monospace'],
    },
  },
  plugins: [],
};
