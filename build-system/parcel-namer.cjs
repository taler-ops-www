const { Namer } = require('@parcel/plugin');
const path = require('path');

const dirs = {
  js: ['js', 'ts', 'jsx', 'tsx'],
  css: ['css', 'scss', 'sass'],
  images: ['png', 'jpg', 'jpeg', 'svg', 'ico', 'bmp'],
  fonts: ['woff2', 'woff', 'ttf', 'eot'],
};

// Reverse into indexable
const dirsByType = {};
for (const [dir, types] of Object.entries(dirs))
  if (typeof types === 'string') dirsByType[types] = dir;
  else for (const type of types) dirsByType[type] = dir;

// Name shit
module.exports = new Namer({
  name({ bundle }) {
    if (dirsByType[bundle.type]) {
      const { filePath } = bundle.getMainEntry();
      const baseName = path.basename(filePath);
      const extName = path.extname(baseName);
      let filename = baseName.substring(0, baseName.length - extName.length);
      if (!bundle.needsStableName) filename += '.' + bundle.hashReference;
      filename += '.' + bundle.type;
      let result = path.join(dirsByType[bundle.type], filename);
      if (!bundle.needsStableName) result = path.join('immutable', result);
      return result;
    }

    return null;
  },
});
