const { Resolver } = require('@parcel/plugin');
const path = require('path');
const fs = require('fs');

module.exports = new Resolver({
  resolve({ specifier }) {
    if (specifier.startsWith('/')) {
      const filePath = path.resolve('rendered', specifier.substring(1));
      if (fs.existsSync(filePath))
        return {
          filePath,
        };
    }
    return null;
  },
});
