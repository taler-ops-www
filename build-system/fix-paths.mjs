import fs from 'fs';
import path from 'path';

const distDir = path.resolve('dist');
console.log('Applying patches to', distDir);

const recursiveReaddirSync = (p) => {
  let list = [],
    files = fs.readdirSync(p),
    stats;

  files.forEach((filepath) => {
    stats = fs.lstatSync(path.join(p, filepath));
    if (stats.isDirectory())
      list = list.concat(recursiveReaddirSync(path.join(p, filepath)));
    else list.push(path.join(p, filepath));
  });

  return list;
};

const distDirRead = recursiveReaddirSync(distDir).filter((v) =>
  fs.statSync(v).isFile(),
);

distDirRead.forEach((targetFile) => {
  const oldContent = fs.readFileSync(targetFile, 'utf-8');
  const chkResult = oldContent.includes('BASE_URL_RELATIVE_TO_FILE');
  if (process.env.DEBUG_READDIR_CHK_RES)
    console.log('CHK', targetFile, '=>', chkResult);
  if (chkResult) {
    const newContent = oldContent.replace(
      /BASE_URL_RELATIVE_TO_FILE/giu,
      path.relative(path.join(targetFile, '..'), distDir),
    );
    console.log('Patching file', targetFile);
    fs.writeFileSync(targetFile, newContent);
  }
});
