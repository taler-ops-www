document
  .querySelectorAll('.toggleable > .toggle-trigger')
  .forEach((trigger) => {
    const toggleable = trigger.parentElement;
    const content = toggleable?.querySelector('.toggle-content');
    if (!toggleable || !content)
      return console.warn(
        'trigger',
        trigger,
        'resulted in error:',
        new Error(
          'could not resolve one of: toggleable parent, toggleable content',
        ),
      );

    trigger.addEventListener('click', () => {
      content.classList.toggle('hidden');
    });
  });
