type Key = 'firefox' | 'chrome' | 'android' | 'ios'; // an enum might be better but its 6am i havent slept for 18hrs ill deal with this later

// Declare the group selection function as a global
declare const selectGroup: (key: Key, scroll?: boolean) => void;

const main = async () => {
  // Scope everything to not expose things that arent necessary
  const disableGroup = (key: Key) => {
    const group = document.querySelectorAll('.group-' + key);
    group.forEach((element) =>
      (element as HTMLElement).classList.add('hidden'),
    );
    document
      .querySelectorAll(
        `[data-vendor-brand-path-element=${JSON.stringify(key)}]`,
      )
      .forEach((v) => v.setAttribute('fill', '#777'));
  };

  const disableAll = () => {
    disableGroup('android');
    disableGroup('chrome');
    disableGroup('firefox');
    disableGroup('ios');
  };

  const selectGroup = (key: Key, scroll = true) => {
    disableAll();
    const group = document.querySelectorAll('.group-' + key);
    group.forEach((element) => {
      (element as HTMLElement).classList.remove('hidden');
    });
    document
      .querySelectorAll(
        `[data-vendor-brand-path-element=${JSON.stringify(key)}]`,
      )
      .forEach((v) => v.setAttribute('fill', '#0042B3'));
    const firstSection = document.querySelector('[data-is-first-section]');
    if (scroll && firstSection)
      document.documentElement.scrollTo({
        behavior: 'smooth',
        top:
          (firstSection as HTMLElement).offsetTop -
          ((document
            .querySelector('header.navigation-menu')
            ?.getBoundingClientRect().height ?? 0) +
            32),
      });
  };

  // Assign selectGroup to the global scope
  globalThis['selectGroup' as any] = selectGroup;

  try {
    const { default: UA } = await import('./vendored/ua');
    // Call it based on UA
    const ua = new UA();
    switch (true) {
      case ua.os === UA.OS.Android:
        selectGroup('android', false);
        break;
      case ua.os === UA.OS.iOS:
        selectGroup('ios', false);
        break;
      case ua.browser === UA.Browser.Firefox:
        selectGroup('firefox', false);
        break;
      case ua.browser === UA.Browser.Chromium:
        selectGroup('chrome', false);
        break;
      default:
        selectGroup('firefox', false);
        break;
    }
  } catch (error) {
    console.warn('Failed to load UA Detection:', error);
    // fall back to ff
    selectGroup('firefox', false);
  }
};
main();
