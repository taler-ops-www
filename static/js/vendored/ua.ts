// Platform Detection using Enums & Proper OOP
// License: GPL (upstream is Apache-2.0)
// Upstream: https://github.com/leondejong/platform-detection

export enum Browser {
  Chrome = 'chrome',
  Chromium = 'chrome',
  Firefox = 'firefox',
  Opera = 'opera',
  Edge = 'edge',
  Safari = 'safari',
  IE = 'explorer',
  Unknown = '',
}
export enum OS {
  Android = 'android',
  iOS = 'ios',
  Linux = 'linux',
  MacOS = 'macos',
  Win32 = 'win32',
  Unknown = '',
}
export enum Device {
  Desktop = 'desktop',
  Tablet = 'tablet',
  Mobile = 'mobile',
  Unknown = '',
}

export default class Platform {
  public static readonly Device = Device;
  public static readonly OS = OS;
  public static readonly Browser = Browser;

  public device = Device.Unknown;
  public os = OS.Unknown;
  public browser = Browser.Unknown;

  public constructor(public userAgent = navigator.userAgent) {
    this.update(userAgent);
  }

  public update(userAgent = navigator.userAgent) {
    this.userAgent = userAgent;

    switch (true) {
      case this.isTablet():
        this.device = Device.Tablet;
        break;
      case this.isMobile():
        this.device = Device.Mobile;
        break;
      case this.isDesktop():
        this.device = Device.Desktop;
        break;
      default:
        this.device = Device.Unknown;
        break;
    }
    switch (true) {
      case this.isAndroid():
        this.os = OS.Android;
        break;
      case this.isIOS():
        this.os = OS.iOS;
        break;
      case this.isLinux():
        this.os = OS.Linux;
        break;
      case this.isMacOS():
        this.os = OS.MacOS;
        break;
      case this.isWin32():
        this.os = OS.Win32;
        break;
      default:
        this.os = OS.Unknown;
        break;
    }
    switch (true) {
      case this.isChrome():
        this.browser = Browser.Chromium;
        break;
      case this.isFirefox():
        this.browser = Browser.Firefox;
        break;
      case this.isOpera():
        this.browser = Browser.Opera;
        break;
      case this.isEdge():
        this.browser = Browser.Edge;
        break;
      case this.isSafari():
        this.browser = Browser.Safari;
        break;
      case this.isIE():
        this.browser = Browser.IE;
        break;
      default:
        this.browser = Browser.Unknown;
        break;
    }
  }

  /** Adds OS, Device and Browser classes to the body */
  public addClasses() {
    if (this.device) document.body.classList.add(this.device);
    if (this.os) document.body.classList.add(this.os);
    if (this.browser) document.body.classList.add(this.browser);
  }

  /** Checks if the user is on desktop (true if not mobile and not tablet) */
  protected isDesktop(): boolean {
    return !this.isMobile() && !this.isTablet();
  }

  /** Checks if the user is on a tablet or not */
  protected isTablet(): boolean {
    return /tablet|ipad/i.test(this.userAgent);
  }

  /** Checks if the user is on a list of known phones */
  protected isMobile(): boolean {
    return /mobile|iphone|ipod|android|windows *phone/i.test(this.userAgent);
  }

  /** Checks if the user is on android */
  protected isAndroid(): boolean {
    return /android/i.test(this.userAgent);
  }

  /** Checks if the user is on iOS */
  protected isIOS(): boolean {
    return /ipad|iphone|ipod/i.test(this.userAgent);
  }

  /** Checks if the user is on Linux */
  protected isLinux(): boolean {
    return /linux/i.test(this.userAgent);
  }

  /** Checks if the user is on Macos/MacOS */
  protected isMacOS(): boolean {
    return /macintosh|os *x/i.test(this.userAgent);
  }

  /**
   * Checks if the user is on a Windows User-Agent
   * Note: Name is misleading; Windows refers to itself as Win32 on x64 aswell.
   */
  protected isWin32(): boolean {
    return /windows|win64|win32/i.test(this.userAgent);
  }

  /** Checks if we're chrome/chromium based */
  protected isChromiumBased(): boolean {
    return /chrome|chromium/i.test(this.userAgent);
  }
  /** Checks if we're chrome/chromium but not a fork thereof */
  protected isChrome(): boolean {
    return this.isChromiumBased() && !this.isOpera() && !this.isEdge();
  }

  /** Checks if we're on firefox */
  protected isFirefox(): boolean {
    return /firefox/i.test(this.userAgent);
  }

  /** Checks if we're on opera */
  protected isOpera(): boolean {
    return /opr/i.test(this.userAgent);
  }

  /** Chceks if we're on Edge */
  protected isEdge(): boolean {
    return /edge/i.test(this.userAgent);
  }

  /** Checks if we're on Safari */
  protected isSafari(): boolean {
    return (
      /safari/i.test(this.userAgent) &&
      !this.isChrome() &&
      !this.isOpera() &&
      !this.isEdge()
    );
  }

  /** Checks if we're on IE/pre-chromium Edge */
  protected isIE(): boolean {
    return /msie|trident/i.test(this.userAgent);
  }
}
