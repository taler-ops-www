# GNUplot script to generate the Stefan curve
# This file is in the public domain.
set xlabel 'Net amount (CHF)'
set ylabel 'Deposit fees (CHF)'
set xrange [0:100]
set yrange [0:0.08]
set size 1.0, 1.0
set terminal svg enhanced
set output 'stefan.svg'
plot 0.0025 + log(x)/log(2) * 0.01 title 'Customary fees'
