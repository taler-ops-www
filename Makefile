# This file is in the public domain.

include build-system/config.mk

# All: build HTML pages in all languages.
.PHONY: all
all: build-site parcel optimize-css

# build-unoptimized: Builds without optimizing CSS as to allow for faster testing
build-unoptimized: build-site parcel-no-optimize

build-site:
	./inc/update-messages
	env "BASEURL=$(opt_baseurl)" ./inc/build-site

optimize-css:
	./build-system/pkgmgr run optimize-css;

parcel: build-parcel postbuild-parcel
parcel-no-optimize: build-parcel-no-optimize postbuild-parcel

build-parcel:
	./build-system/pkgmgr run build-parcel;
build-parcel-no-optimize:
	./build-system/pkgmgr run build-parcel-no-optimize;
fix-parcel-paths:
	./build-system/pkgmgr run fix-parcel-paths;
postbuild-parcel: fix-parcel-paths

.PHONY: run
run: all
	$(browser) http://0.0.0.0:8000/en &
	$(python) -m http.server --directory dist/

variant = $(opt_variant)

ifndef variant
$(error variant is not set)
endif

.PHONY: install
install: all install-files

# install-dev: Same functionality as install, but using build-unoptimized for faster build times
install-dev: build-unoptimized install-files

# install-files: The internal install task
install-files:
	$(mkdir) -p $(prefix)/$(variant)
	$(cp) -r rendered/* $(prefix)/$(variant)/
	$(cp) -r rendered/.well-known/ $(prefix)/$(variant)/
	$(cp) -r dist/* $(prefix)/$(variant)/

.PHONY: clean
clean:
	$(rm) -rf .parcel-cache/
	$(rm) -rf __pycache__ *.pyc  *~ \.*~ \#*\#
	$(rm) -rf rendered/
	$(rm) -rf dist/

.PHONY: submodules/update
submodules/update:
	$(git) submodule update --recursive --remote
