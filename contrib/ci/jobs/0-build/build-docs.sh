#!/bin/bash

set -exou

./bootstrap
./configure --prefix=/artifacts/taler-ops-www/${CI_COMMIT_REF} # Variable comes from CI environment
make install
