#!/bin/bash
set -exuo pipefail

ARTIFACT_PATH="/artifacts/taler-ops-www/${CI_COMMIT_REF}/*"

SCP_HOST="talerops@anastasis.lu"
SCP_PATH="tops-stage/stage-talerops-public"
SCP_DEST="${SCP_HOST}:${SCP_PATH}"

ls $ARTIFACT_PATH

apt-get update -yqq
apt-get install -yqq --no-install-recommends \
	ssh-client

scp -rvp \
      ${ARTIFACT_PATH} ${SCP_DEST}
